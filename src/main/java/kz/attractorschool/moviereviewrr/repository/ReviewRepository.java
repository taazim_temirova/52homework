package kz.attractorschool.moviereviewrr.repository;

import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.model.Review;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepository extends CrudRepository<Review, String> {

    public Iterable<Review> findAll(Sort s);
    public  Iterable<Review> findAllById(Sort s);

    @Query("{'stars' : { '$gte' : ?0, '$lte' : ?1 }}")
    public Iterable<Review> getRewiewBetween(int stars, int stars2, Sort s);

}
