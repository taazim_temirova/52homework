package kz.attractorschool.moviereviewrr.repository;

import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    public Iterable<User> findAll(Sort s);

    @Query("{'name' : { '$gte' : ?0, '$lte' : ?1 }}")
    public Iterable<User> findAllByName(String name, Sort s);


}
