package kz.attractorschool.moviereviewrr.controller;

import kz.attractorschool.moviereviewrr.model.Movie;
import kz.attractorschool.moviereviewrr.model.Review;
import kz.attractorschool.moviereviewrr.model.User;
import kz.attractorschool.moviereviewrr.repository.MovieRepository;
import kz.attractorschool.moviereviewrr.repository.ReviewRepository;
import kz.attractorschool.moviereviewrr.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    MovieRepository mr;

    @Autowired
    ReviewRepository rr;

    @Autowired
    UserRepository ur;

    @GetMapping("/movie")
    public Iterable<Movie> getMovie() {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return mr.findAll(s);
    }

    @GetMapping("/user")
    public  Iterable<User> getUser(){
        Sort s = Sort.by(Sort.Order.desc("name"));
        return ur.findAll(s);
    }



    @GetMapping("/rewiew")
    public Iterable<Review> getRewiew(){
        Sort s = Sort.by(Sort.Order.asc("rewiew"));
        return  rr.findAll(s);
    }

    @GetMapping("/remiew/{id}")
    public  Iterable<Review> getRewiewId(){
        Sort s = Sort.by(Sort.Order.asc("_id"));
        return  rr.findAllById(s);
    }

    @GetMapping("/movienew/{year}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return mr.findAllByReleaseYearGreaterThanEqual(year, s);
    }

    @GetMapping("/movie/{actors}")
    public  Iterable<Movie> getActor(@PathVariable("actors") String actors){
        Sort s = Sort.by(Sort.Order.asc("actors"));
        return  mr.findAllByActors(actors,s);

    }

    @GetMapping("/user/{name}")
    public Iterable<User> getUser(@PathVariable("name") String name){
        Sort s = Sort.by(Sort.Order.asc("name"));
        return  ur.findAllByName(name,s);
    }

    @GetMapping("/movienew/{year}/{year2}")
    public Iterable<Movie> getMovie(@PathVariable("year") int year,
                                    @PathVariable("year2") int year2) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return mr.findAllByReleaseYearBetween(year, year2, s);
    }

    @GetMapping("/moviebetween/{year}/{year2}")
    public Iterable<Movie> getMovieBetween(@PathVariable("year") int year,
                                           @PathVariable("year2") int year2) {
        Sort s = Sort.by(Sort.Order.asc("title"));

        return mr.getMoviesBetween(year, year2, s);
    }
    @GetMapping("/rewiewbetween/{stars}/{stars2}")
    public Iterable<Review> getRewiewBetween(@PathVariable("stars") int stars,
                                            @PathVariable("stars2") int stars2) {
    Sort s = Sort.by(Sort.Order.asc("stars"));
    return rr.getRewiewBetween(stars,stars2,s);
    }



}
